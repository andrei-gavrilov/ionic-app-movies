import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-book-details',
  templateUrl: 'book-details.html',
})
export class BookDetailsPage {

    public book: any = {};
    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.book=navParams.get('book');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BookDetailsPage');
    }

}
