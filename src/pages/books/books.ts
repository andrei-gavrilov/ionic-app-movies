import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { BookDetailsPage } from '../book-details/book-details';

/**
 * Generated class for the BooksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-books',
  templateUrl: 'books.html',
})
export class BooksPage {
    books = [];
    public genre: any = {};

    constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, public navParams: NavParams) {

        this.genre=navParams.get('genre');

        this.databaseprovider.getDatabaseState().subscribe(rdy => {
            if (rdy) {
              this.loadBooks();
            }
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BooksPage');
    }
    loadBooks() {
        this.databaseprovider.getAllBooksByGenreId(this.genre.id).then(data => {
            this.books = data;
        })
    }
    public bookClicked(event, book){
        this.navCtrl.push(BookDetailsPage,{
            book: book
        });
    }

}
