
import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { BooksPage } from '../books/books';


/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
    genres = [];
    constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
        this.databaseprovider.getDatabaseState().subscribe(rdy => {
          if (rdy) {
            this.loadGenres();
          }
        })

    }
    loadGenres() {
        this.databaseprovider.getAllGenres().then(data => {
            this.genres = data;
        })
    }
    public genreClicked(event, genre){
        this.navCtrl.push(BooksPage,{
            genre: genre
        });
    }
}