CREATE TABLE IF NOT EXISTS genre (id INTEGER PRIMARY KEY, name TEXT);
CREATE TABLE IF NOT EXISTS book (id INTEGER PRIMARY KEY, name TEXT, description TEXT, release_year TEXT);
CREATE TABLE IF NOT EXISTS genre_book (
                        genre_id INTEGER,
                        book_id INTEGER,
                        FOREIGN KEY(genre_id) REFERENCES genre(id),
                        FOREIGN KEY(book_id) REFERENCES book(id)
                );

INSERT INTO genre(id, name) VALUES (1, 'Romance');
INSERT INTO genre(id, name) VALUES (2, 'Fantasy');
INSERT INTO genre(id, name) VALUES (3, 'Sci-fy');

INSERT INTO book(id, name, description, release_year) VALUES (1, 
                        'Sword of Destiny', 
                        'A collection of stories in the world of Geralt the Witcher, introducing some of the most loved characters from this universe. A must-read for fans of the novels and games alike.',
                        '1992');
INSERT INTO book(id, name, description, release_year) VALUES (2, 
                        'The Little Prince', 
                        'Moral allegory and spiritual autobiography, The Little Prince is the most translated book in the French language. With a timeless charm it tells the story of a little boy who leaves the safety of his own tiny planet to travel the universe, learning the vagaries of adult behaviour through a series of extraordinary encounters.',
                        '2000');
INSERT INTO book(id, name, description, release_year) VALUES (3, 
                        'Ender''s Game', 
                        'Andrew "Ender" Wiggin thinks he is playing computer simulated war games; he is, in fact, engaged in something far more desperate. The result of genetic experimentation, Ender may be the military genius Earth desperately needs in a war against an alien enemy seeking to destroy all human life. The only way to find out is to throw Ender into ever harsher training, to chip away and find the diamond inside, or destroy him utterly. Ender Wiggin is six years old when it begins. He will grow up fast.',
                        '1994');

INSERT INTO genre_book(genre_id, book_id) VALUES (1, 1);
INSERT INTO genre_book(genre_id, book_id) VALUES (2, 1);
INSERT INTO genre_book(genre_id, book_id) VALUES (2, 2);
INSERT INTO genre_book(genre_id, book_id) VALUES (3, 3);