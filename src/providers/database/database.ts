import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

    database: SQLiteObject;
    private databaseReady: BehaviorSubject<boolean>;
   
    constructor(public sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform, private http: Http) {
      this.databaseReady = new BehaviorSubject(false);
      this.platform.ready().then(() => {
        this.sqlite.create({
          name: 'books.db',
          location: 'default'
        })
          .then((db: SQLiteObject) => {
            this.database = db;
            this.storage.get('database_filled').then(val => {
              if (val) {
                this.databaseReady.next(true);
              } else {
                this.fillDatabase();
              }
            });
          });
      });
    }
    fillDatabase() {
        this.http.get('assets/books.sql')
          .map(res => res.text())
          .subscribe(sql => {
            this.sqlitePorter.importSqlToDb(this.database, sql)
              .then(data => {
                this.databaseReady.next(true);
                this.storage.set('database_filled', true);
              })
              .catch(e => console.error(e));
          });
      }

      getAllGenres() {
        return this.database.executeSql("SELECT * FROM genre", []).then((data) => {
          let genres = [];
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
                genres.push({ id: data.rows.item(i).id, name: data.rows.item(i).name});
            }
          }
          return genres;
        }, err => {
          console.log('Error: ', err);
          return [];
        });
      }

      getAllBooksByGenreId(id) {
        return this.database.executeSql("SELECT * FROM book INNER JOIN genre_book ON book.id=genre_book.book_id WHERE genre_id=?;", [id]).then((data) => {
          let books = [];
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
                books.push({ id: data.rows.item(i).id, name: data.rows.item(i).name, description: data.rows.item(i).description, release_year: data.rows.item(i).release_year});
            }
          }
          return books;
        }, err => {
          console.log('Error: ', err);
          return [];
        });
      }
     
      getDatabaseState() {
        return this.databaseReady.asObservable();
      }
     
    }

